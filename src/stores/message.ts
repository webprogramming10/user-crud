import { ref } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("Message", () => {
  const isShow = ref(false);
  const message = ref("");
  const timeOut = ref(2000);
  const showMes = (msg: string, tout: number = 2000) => {
    message.value = msg;
    isShow.value = true;
    timeOut.value = tout;
  };
  const closeMes = () => {
    message.value = "";
    isShow.value = false;
  };
  return { isShow, message, showMes, closeMes, timeOut };
});
